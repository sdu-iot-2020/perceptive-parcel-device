from network import Sigfox
from socket import socket, SOCK_RAW, AF_SIGFOX, SOL_SIGFOX, SO_RX, SO_BIT
from errno import ENETDOWN
from time import sleep
from machine import Timer
from SI7006A20 import SI7006A20
from MPL3115A2 import MPL3115A2
from LIS2HH12 import LIS2HH12, FULL_SCALE_8G
from sys import stderr
from pycom import rgbled

REQUEST_DOWLINK_INTERVAL = 2*60
UPLOAD_INTERVAL = 1*60
TILT_THRESHOLD = 80
ACC_THRESHOLD = 1.5

sigfox = Sigfox(mode=Sigfox.SIGFOX, rcz=Sigfox.RCZ1)

temp = SI7006A20()
# bar = MPL3115A2()
acc = LIS2HH12()


def requst_dowlink(shaken: bool, tilted: bool) -> None:
    try:
        sock = socket(AF_SIGFOX, SOCK_RAW)
        sock.setblocking(True)
        sock.setsockopt(SOL_SIGFOX, SO_RX, True)

        t = int(temp.temperature())
        h = int(temp.humidity())

        b = 0

        if tilted:
            b = b ^ 128
        if shaken:
            b = b ^ 64

        m = bytes([t, h, b])
        sock.send(m)

        r = sock.recv(8)
        print('recv', r)
        if r == b'\xde\xad\xbe\xef\xca\xfe\xba\xbe':
            rgbled(0x007f00)
    except OSError as e: 
        if e == ENETDOWN:
            print(e)
    finally:
        sock.close()

alarm = Timer.Alarm(
    handler=lambda a: requst_dowlink(0, 0),
    s=UPLOAD_INTERVAL,
    periodic=True
)

while True:
    shaken = 0
    tilted = 0

    x, y, z = acc.acceleration()
    if x > ACC_THRESHOLD or y > ACC_THRESHOLD or z > ACC_THRESHOLD:
        shaken = 1
        print('Parcel shaken')

    p = acc.pitch()
    r = acc.roll()
    if p > TILT_THRESHOLD or p < -TILT_THRESHOLD or r > TILT_THRESHOLD or r < -TILT_THRESHOLD:
        tilted = 1
        print('Parcel tilted')

    if shaken == 1 or tilted == 1:
        requst_dowlink(shaken, tilted)

    sleep(1)
